import os
from dirsync import sync

def sendFile(ftp,fullFilePath):
    ftp.storlines("STOR " + os.path.basename(fullFilePath), open(fullFilePath))

def receiveFile(ftp,fullFilePath):
    handle = open(os.path.basename(fullFilePath), 'wb')
    ftp.retrbinary('RETR %s' % os.path.basename(fullFilePath), handle.write)
    handle.close()

def deleteFile(ftp,fullFilePath):
    ftp.delete(os.path.basename(fullFilePath))


def syncFileDir(ftp,localPath, remotePath):
    sync(remotePath,localPath,"sync")
    # _local_files = []
    # _remote_files = []
    #
    # if os.listdir(localPath) == []:
    #     print 'LOCAL DIR IS EMPTY'
    # else:
    #     print 'BUILDING LOCAL DIR FILE LIST...'
    #     for file_name in os.listdir(localPath):
    #         _local_files.append(file_name)
    #
    # print 'BUILDING REMOTE DIR FILE LIST...\n'
    #
    # for rfile in ftp.nlst():
    #     if rfile.endswith('.txt'):
    #         _remote_files.append(rfile)
    #
    # _diff = sorted(list(set(_remote_files) - set(_local_files)))
    #
    # for h in _diff:
    #     with open(os.path.join(localPath,h), 'wb') as ftpfile:
    #         s = ftp.retrbinary('RETR ' + h, ftpfile.write) # retrieve file
    #         print 'PROCESSING', h
    #         if str(s).startswith('226'): # comes from ftp status: '226 Transfer complete.'
    #             print 'OK\n' # print 'OK' if transfer was successful
    #         else:
    #             print s # if error, print retrbinary's return
