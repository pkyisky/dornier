
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt, csrf_protect

from wifiCon import *
from fileHandling import *
import os
import ftplib

_remotePath = r"\Storage Card"
controlUnitIP = "169.254.242.73"

def getFTP():
    ftp = ftplib.FTP(controlUnitIP)
    ftp.login("", "")
    ftp.cwd(_remotePath)




def connectView(request):
    _connectionFile = "connection.config"
    handle = open(_connectionFile, 'wb')
    ftp = getFTP()
    ftp.retrbinary('RETR %s' % _connectionFile, handle.write)
    handle.close()
    with open(_connectionFile,'r') as f:
        for cnt, line in enumerate(f):
            # print "line", line
            if cnt ==0:
                data = line.split('=')
                SSID = data[1][0:-1]

                print 'SSID :', SSID, len(SSID)
            if cnt == 1:
                data = line.split('=')
                PASS = data[1][0:-1]
                print "PASS :", PASS, len(PASS)

    Connect(SSID,PASS)
    return JsonResponse({"status" : "success"})

def discoverWiFiNetworksView(request):

    ftp = getFTP()
    _wiFiList = Search()
    _wifiListFile = os.path.join(os.getcwd() , "ESSID")

    with open(_wifiListFile, 'w') as f:
        for cell in _wiFiList:
            cell.summary = 'SSID={}, Quality={}'.format(cell.ssid, cell.quality)

            if cell.encrypted:
                enc_yes_no = 'Protected'
            else:
                enc_yes_no = 'Unprotected'

            cell.summary = cell.summary + ', Encryption={}'.format(enc_yes_no)
            f.write('%s\n' %cell.summary)
            print cell.summary

    ftp.storlines("STOR " + os.path.basename(_wifiListFile), open(_wifiListFile))

    return JsonResponse({"status" : "success"})


@csrf_exempt
def createPatientView(request):
    ftp = getFTP()
    if request.method == 'POST':
        filePath = 'patient' + str(request.POST['patientID'])

        f = open(filePath , 'w')

        f.write("firstName="  +   request.POST['firstName'] + '\n')
        f.write("lastName="  +   request.POST['lastName'] + '\n')
        f.write("patientID="  +   request.POST['patientID'] + '\n')

        f.close()
        # ftp = ftplib.FTP(controlUnitIP)
        # ftp.login("", "")
        # ftp.cwd(_remotePath)

        _fullFilePath = os.path.join(os.getcwd() , filePath)
        ftp.storlines("STOR " + os.path.basename(_fullFilePath), open(_fullFilePath))

        #so the sftp
        # try:
        #     # sendFile(ftp,_fullFilePath)
        #
        # except Exception as e:
        #     pass

        data = {
        'status': 'success',
        }
        return JsonResponse(data)

@csrf_exempt
def deviceInfoView(request):
    data = {
        'deviceName': 'DEORNIER101',
        'ip': request.META['REMOTE_ADDR'],
        'deviceID': "343242sdsad2321",
    }
    return JsonResponse(data)
