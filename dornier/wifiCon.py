import wifi
import subprocess
_interface = 'wlan0'
def Search():
    wifilist = []

    cells = wifi.Cell.all(_interface)
    # print 'cells : ', cells

    for cell in cells:
        wifilist.append(cell)

    return wifilist


def FindFromSearchList(ssid):
    # wifilist = Search()
    wifilist = []

    cells = wifi.Cell.all(_interface)
    # print 'cells : ', cells

    for cell in cells:
        wifilist.append(cell)

    print 'wifiList :', wifilist
    for cell in wifilist:
        if cell.ssid == ssid:
            return cell

    return False


def FindFromSavedList(ssid):
    cell = wifi.Scheme.find(_interface, ssid)
    print 'Saved cell :', cell

    if cell:
        return cell

    return False

def Connect(ssid, password):
    command = 'nmcli dev wifi connect {} password {}'.format(ssid,password)
    process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
    process.wait()

# def Connect(ssid, password=None):
#     cell = FindFromSearchList(ssid)
#     print "Start connecting to CIOC and cell :", cell
#     if cell:
#         savedcell = FindFromSavedList(cell.ssid)
#         print 'savedcell :', savedcell
#         # Already Saved from Setting
#         if savedcell:
#             print 'activatting \n'
#             savedcell.activate()
#
#             return cell
#
#         # First time to conenct
#         else:
#             if cell.encrypted:
#                 if password:
#                     scheme = Add(cell, password)
#
#                     try:
#                         scheme.activate()
#
#                     # Wrong Password
#                     except wifi.exceptions.ConnectionError:
#                         Delete(ssid)
#                         return False
#
#                     return cell
#                 else:
#                     return False
#             else:
#                 scheme = Add(cell)
#
#                 try:
#                     scheme.activate()
#                 except wifi.exceptions.ConnectionError:
#                     Delete(ssid)
#                     return False
#
#                 return cell
#
#     return False


def Add(cell, password=None):
    if not cell:
        return False

    scheme = wifi.Scheme.for_cell(_interface, cell.ssid, cell, password)
    scheme.save()
    return scheme


def Delete(ssid):
    if not ssid:
        return False

    cell = FindFromSavedList(ssid)

    if cell:
        cell.delete()
        return True

    return False
